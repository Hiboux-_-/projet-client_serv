
package comClient_Serv;

import java.util.Objects;

public class AddressServerTcp {
    
    private String ip;
    private int port;
	
    public AddressServerTcp(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }
    
    public String getIp () {
        return this.ip;
    }

    public int getPort () {
        return this.port;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.ip);
        hash = 79 * hash + this.port;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AddressServerTcp other = (AddressServerTcp) obj;
        if (this.ip != other.ip) {
            return false;
        }
        if (this.port != other.port) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ip+":"+port;
    }
}