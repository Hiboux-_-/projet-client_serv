package comClient_Serv;

import java.util.*;
import java.util.Map.*;

public class ListFilesServer {

    private Map<P2PFile, ArrayList<AddressServerTcp> > ListFile;
    
    public ListFilesServer(){
        this.ListFile = new HashMap<P2PFile, ArrayList<AddressServerTcp> >() {};

    }
    
    public void add(AddressServerTcp client, ArrayList<P2PFile> ListFileClient){
        ArrayList<AddressServerTcp> ListClient = new ArrayList();
        ListClient.add(client);
        for(P2PFile f : ListFileClient){
            if(!ListFile.containsKey(f)){
                ListFile.put(f,ListClient);
            } else if(!ListFile.get(f).contains(client)){
                ListFile.get(f).add(client);
                System.out.println("Client Ajouter : "+client);
            }
        }
    }
    
    public void remove (AddressServerTcp client, ArrayList<P2PFile> ListFileClient){
        
        for(P2PFile f : ListFileClient){
            ListFile.get(f).remove(client);
            if(ListFile.get(f).isEmpty()){
                ListFile.remove(f);
            }
        }
    }
    
    public void remove (AddressServerTcp client){
        
        ArrayList<P2PFile> Empty = new ArrayList();
        for(Entry<P2PFile, ArrayList<AddressServerTcp>> entry : ListFile.entrySet()){

            P2PFile cle = entry.getKey();
            ArrayList<AddressServerTcp> value = entry.getValue();
            value.remove(client);
            if(value.size() == 0){
                Empty.add(cle);
            }
        }
        for(P2PFile f : Empty){
            if(ListFile.get(f).isEmpty()){
                ListFile.remove(f);
            }
        }
        
    }
    
    /**
     * Recherche d'un fichier via un pattern dans la Liste des fichiers connue par le Serveur
     * 
     * @param pattern Pattern qui sert a match avec tout les nom de fichiers connue par le Serveur
     * @return  Retourne la Liste des fichiers qui ont Matcher avec le Pattern
     */
    public ArrayList<P2PFile> Search(String pattern){
        ArrayList<P2PFile> result = new ArrayList<>();
        
        for(Entry<P2PFile, ArrayList<AddressServerTcp>> entry : ListFile.entrySet()){

            P2PFile cle = entry.getKey();
            String nameFile = cle.getName();
            if(nameFile.matches(".*"+pattern+".*")){
                result.add(cle);
            }
        }
        return result;
    }
    
    
    
    
    /**
     * Permet d'obtenir tout les addressServerTcp d'un fichier donnée
     * @param File Fichier dont on veut obtenir les addressServerTcp
     * @return Retourne la Liste des AddressServerTcp du fichier donnée
     */
    public ArrayList<AddressServerTcp> getAdress(String File){
        ArrayList<AddressServerTcp> result = new ArrayList<>();
        
        for(Entry<P2PFile, ArrayList<AddressServerTcp>> entry : ListFile.entrySet()){
            P2PFile cle = entry.getKey();
            List<AddressServerTcp> value = entry.getValue();
            String nameFile = cle.getName();
            if(nameFile.matches(File+".*")){
                result.addAll(value);
            }
        }
        return result;
    }
    
    
    // <TreeSet<P2PFile>,AddressServerTcp>
    // Ajout de tout les fichiers d'un utilisateur
    // Suppression de tout les fichiers d'un utilisateur 

    @Override
    public String toString() {
       StringBuilder result = new StringBuilder();
       
       for(Entry<P2PFile, ArrayList<AddressServerTcp>> entry : ListFile.entrySet()){
            P2PFile cle = entry.getKey();
            List<AddressServerTcp> value = entry.getValue();
            result.append(cle.getName()).append(" : ");
            for(AddressServerTcp v : value){
                result.append(v).append(",");
            }
            result.append("\n");
        }
       return result.toString();
    }
}
