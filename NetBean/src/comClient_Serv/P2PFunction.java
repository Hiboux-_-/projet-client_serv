package comClient_Serv;

public class P2PFunction {
    
    /**
     * Calcule le nombre de morceaux de fichiers necessaires a telecharger
     * 
     * @param size
     * @return Retourne la valeur calculee
     */
    public int calculNbMorceauTotal (int size) {
        return ((size % 1024) == 0) ? (size / 1024) : (size / 1024) + 1 ;
    }
    
    /**
     * Calcule le nombre de morceaux a recuperer sur chaque client
     * 
     * @param total
     * @param nbClient
     * @return Retourne la valeur calculee
     */
    public int calculNbMorceauClient (int total, int nbClient) {
        return total/nbClient;
    }
    
}
