package P2PClient;

import java.net.*;
import comClient_Serv.*;
import java.io.*;
import java.nio.charset.Charset;

public class ThreadClient extends Thread {
    
    public ServerSocket sock = null;
    
    public ThreadClient (ServerSocket sock)
    {
        this.sock = sock;
    }
    
    public void run () {
        
        DatagramSocket sockComm = null;
        
        try
        {
            sockComm = new DatagramSocket(sock.accept().getPort());
            
            byte[] buff = new byte[P2PParam.TAILLE_BUFFER];
            
            DatagramPacket pkRecieve = new DatagramPacket(buff, buff.length);
            
            sockComm.receive(pkRecieve);
            
            System.out.println("DEBUG : Requete reçue : " + new String(buff, 0, pkRecieve.getLength(), Charset.defaultCharset()));
            
            int portClient = pkRecieve.getPort();
            String ipServ = null;
            
            System.out.println("DEBUG : Numéro de port de l'application émettrice du datagramme : " + portClient);
            
            String requete = "Test Client";
            byte[] buffRequete = requete.getBytes();
            InetAddress adIpClient = pkRecieve.getAddress();
            DatagramPacket pkSend = new DatagramPacket(buffRequete, buffRequete.length, adIpClient, portClient);
            sockComm.send(pkSend);
            
        } catch (IOException e)
        {
            e.getMessage();
        } finally
        {
            if (sockComm != null)
            {
                sockComm.close();
            }
        }
    }
}