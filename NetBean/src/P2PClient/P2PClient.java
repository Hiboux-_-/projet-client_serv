package P2PClient;

import comClient_Serv.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class P2PClient {
    
    public static void main(String[] args) {

	if (args.length != 3) {
            System.out.println("Usage : java P2PClient IPServeur portServeur PathFile");
            System.exit(1);
	}

	int portServ = 0 ;
	try
        {
            portServ = Integer.parseInt(args[1]); 
	}
	catch (NumberFormatException e)
        {
            System.out.println("Numéro de port non valide !");
            System.exit(1);
	}

	Socket sockComm = null;
        ServerSocket sockConn = null;
                
	ObjectOutputStream out = null;
	ObjectInputStream in = null;
	BufferedReader cmd = null;

	try
        {
            //Creates a stream socket and connects it to the specified port number 
            //at the specified IP address.
            String ipServ = args[0];

            File repertoire = new File(args[2]);
            File[] files = repertoire.listFiles();

            ArrayList<P2PFile> listFileClient = new ArrayList();

            for(File f : files){
                String nameFile = f.getName();
                long sizeFile = f.length();
                listFileClient.add(new P2PFile(nameFile,sizeFile));
                System.out.println("DEBUG "+ new P2PFile(nameFile,sizeFile));
            }

            sockConn = new ServerSocket(0);

            sockComm = new Socket(ipServ,portServ);
            cmd = new BufferedReader(new InputStreamReader(System.in));

            InputStream is = sockComm.getInputStream();
            in = new ObjectInputStream(is);

            OutputStream os = sockComm.getOutputStream();
            out = new ObjectOutputStream(os);
            out.writeObject(listFileClient);
            out.flush();

            System.out.println("Adresse Socket communication: " + sockComm.getInetAddress().getHostAddress() + " | Port:"+sockComm.getLocalPort());

            String line;
            ListFilesServer listFileServer = null;

            while(true){
                menu();
                line = cmd.readLine();
//*****************************************************************************************************************************************************
                String[] commande = line.split(" ");
                if (commande.length == 1) {
                    switch (commande[0]) {
                        case "search" :
                            requeteRechercheServeur();
                            break;
                        case "list" :
                            try
                            {
                                requeteListeServeur(listFileServer, out, in);
                            } catch (ClassNotFoundException e)
                            {
                                System.out.println("Erreur lors de la reception de la liste");
                            }
                            break;
                            
                        case "quit" :
                            out.writeUTF("quit");
                            out.flush();
                            out.writeObject(listFileClient);        // changer plus tard
                            out.flush();
                            
                            break;
                        default:
                            erreur();
                    }
                } else if (commande.length == 2)
                {
                    if (((commande[0].compareToIgnoreCase("local") == 0) && (commande[1].compareToIgnoreCase("list") == 0))) {
                        listFileClient.toString();
                    } else if (commande[0].compareToIgnoreCase("get") == 0)
                    {
                        if (listFileServer == null)
                        {
                            erreur();
                            try 
                            {
                                int numeroFile = Integer.parseInt(commande[1]); 
                            } catch (NumberFormatException e)
                            {
                                erreur();
                            }
                        }
                        requeteRechercheClient();
                    }
                }
//*****************************************************************************************************************************************************

                if(line.equals("quit"))
                {
                    out.writeUTF("quit");
                    out.flush();
                    break;
                } else
                {
                    out.writeUTF(line);
                    out.flush();
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        finally
        {
            try {
                if (sockComm != null)
                {
                    sockComm.close();
                }
                if (in != null)
                {
                    in.close();
                }
                if (out != null)
                {
                    out.close();
                }
            }
            catch(IOException e) {
                System.out.println("Erreur IO");
            }
        }
    }

    private static void menu() {
        System.out.println("1. Chercher un fichier sur le serveur \t\t\t/ commande : search [expression]\n2. Telecharger un fichier dans la liste \t\t/ commande : get [num]\n3. Reafficher la liste des fichiers disponibles \t/ commande : list\n4. Lister les fichiers sur ce client \t\t\t/ commande : local list\n5. quitter l'application cliente \t\t\t/ commande : quit");
    }
    
    private static void erreur() {
        System.out.println("Erreur lors de la saisie.");
    }
    
    private static void requeteListeServeur (ListFilesServer list, ObjectOutputStream out, ObjectInputStream in) throws IOException, ClassNotFoundException {
        out.writeUTF("list");
        out.flush();
        list = (ListFilesServer) in.readObject();
    }
    
    private static void requeteRechercheServeur () {
        
    }
    
    private static void requeteRechercheClient () {
        
    }
}
