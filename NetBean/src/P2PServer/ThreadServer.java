package P2PServer;

import comClient_Serv.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class ThreadServer extends Thread {
    
    private Socket sockComm = null;
    private ListFilesServer list = null;
    private AddressServerTcp client = null;
    
    public ThreadServer (Socket comm, ListFilesServer list) {
        this.sockComm = comm;
        this.list = list;
        this.client = new AddressServerTcp(comm.getInetAddress().getHostAddress(), comm.getPort());
    }
    
    public void run() {
        ObjectInputStream in = null;
        ObjectOutputStream out = null;
                
        try
        {
            OutputStream os = sockComm.getOutputStream();
            out = new ObjectOutputStream(os);

            
            InputStream is = sockComm.getInputStream();
            in = new ObjectInputStream(is);
/******************************************************************************************************************************************/
            try
            {
                System.out.println("MAJ de la liste de fichier");
                ArrayList<P2PFile> listFileClient = (ArrayList<P2PFile>) in.readObject();
                list.add(client,listFileClient);
                System.out.println(list);
            } catch (ClassNotFoundException e)
            {
                System.out.println("Erreur : Class non trouvee");
            }
            
            String cmd = "";
        //    while(!(cmd = in.readUTF()).equals("quit")){
                cmd = in.readUTF();
                System.out.println("DEBUG : cmd = " + cmd);
                if (cmd.compareToIgnoreCase("list") == 0) {
                    System.out.println("Une demande de liste a ete effectuee");
                }
        //    }
            // Déconnection du client
            
            System.out.println("Le client "+sockComm.getPort()+" s'est déconnecte");
            

                System.out.println("MAJ de la liste de fichier");
                list.remove(client);
                System.out.println(list);

/******************************************************************************************************************************************/
        
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (sockComm != null)
                {
                    sockComm.close();
                }
                if (in != null){
                    in.close();
		}
                if (out != null){
                    out.close();
		}
            } catch(IOException e)
            {
                System.out.println("Erreur lors de la fermeture");
            }
        }
    }
}