package P2PServer;
import comClient_Serv.ListFilesServer;

import java.io.*;
import java.net.*;

public class P2PServer {
    
    public static void main (String[] args) {
        
        if (args.length != 1)
        {
            System.out.println("Usage : java P2PServer portServ");
            System.exit(1);
        }
        
        int portServ = 0;
        
        try
        {
            portServ = Integer.parseInt(args[0]);
        } catch (NumberFormatException e)
        {
            System.out.println("Error : parseInt failed");
            e.getMessage();
            System.exit(2);
        }
        
        ServerSocket sockConn = null;
        Socket sockComm = null;
        ListFilesServer list = new ListFilesServer();
        
        try
        {
            sockConn = new ServerSocket(portServ);
            System.out.println("Address Socket connection: " + sockConn.getInetAddress().getHostAddress());
            
            while(true)
            {
                System.out.println("En attente d'un client.");
                sockComm = sockConn.accept();
                System.out.println("Address Socket communication : " + sockComm.getInetAddress().getHostAddress() + " \t| Port : " + sockComm.getPort());

                ThreadServer serv = new ThreadServer(sockComm, list);

                serv.start();
            } 
/******************************************************************************************************************************************/
            
/******************************************************************************************************************************************/
        
        } catch (IOException e)
        {
            System.out.println("Error IO1");
        } finally
        {
            try
            {
                if (sockComm != null){
                    sockComm.close();
		}
		if (sockConn != null){
                    sockConn.close();
		}
            }
            catch(IOException e)
            {
		System.out.println("Erreur IO2");
            }
	}
    }
}